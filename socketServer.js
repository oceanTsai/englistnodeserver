

var request = require('request');
var io = require('socket.io').listen(3000);
var sentenceChannel = io.of('/sentenceChannel').on('connection', function (client) {
    
    //client指的是對應連線過來的瀏覽器回應物件
    client.on('register', function(sid){
       //this就是client
       try{
            this.join(sid);  //add to room
            this.emit('registerReply', true); //告知瀏覽器端註冊成功
        }catch(e){
            this.emit('registerReply', false);  //告知瀏覽器端註冊失敗
        }
    });   

    //亂數取得詞句
    client.on('getRandomSentence' , function(sid){
      // this.emit('getSentenceReply', sid+'');
      //io.to(sid).emit('getSentenceReply'):
        if(!sid) {
             this.emit('getSentenceReply', null); //因未沒取得sid,所以只能對發送者回應
        }else{
            request.get( {url : 'http://localhost/englishProject/service/randomFind?sessionID='+sid, json : true}, 
                function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        sentenceChannel.to(sid).emit('getSentenceReply', body); //board cast room by session id                                     
                        sentenceChannel.to(sid).emit('hitChangedReply',true); //通知狀態頁要更改數值
                    }else{
                        sentenceChannel.to(sid).emit('getSentenceReply', null);  
                    }
                    
            });
        }
    });

    //增加
    client.on( 'increaseHits', function(data){        
        var self = this;        
        if(!data){
            self.emit('increaseHitsReply', null);
            self = null;
        }else{ 
            var sid = data.sessionID;
            request.post( 'http://localhost/englishProject/service/increaseHits' ,
                {form : {   sessionID : data.sessionID,
                            sentenceID : data.sentenceID,
                            word : data.word
                        }, json : true}, 
                function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        self.emit('increaseHitsReply', body);
                        sentenceChannel.to(sid).emit('hitChangedReply',true); //通知狀態頁要更改數值                 
                    }else{
                         self.emit('increaseHitsReply', null);  
                    }
                    self = null;
                    sid = null;
                }
            );           
        }
    });

    //取閱讀頁所開啟的英文句子
    client.on('getWordList', function(sessionID){
            request.get( {url : 'http://localhost/englishProject/service/getWordList?sessionID='+sessionID, json : true}, 
                function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        sentenceChannel.to(sessionID).emit('getWordListReply', body); //board cast room by session id                        
                    }else{
                        sentenceChannel.to(sessionID).emit('getWordListReply', null);  //null 代表失敗
                    }
            });
    });

    //歸零
    client.on('clearHits' , function(data){
            var self = this;
            var sid =  data.sessionID;
            request.post( 'http://localhost/englishProject/service/clearHits' ,
                {form : {   sessionID : data.sessionID,
                            word : data.word
                        }, json : true}, 
                function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        //self.emit('clearHitsReply', body);
                        sentenceChannel.to(sid).emit('clearHitsReply',body); //通知狀態頁要更改數值                 
                    }else{
                         self.emit('clearHitsReply', null);  
                    }
                    self = null;
                    sid = null;
                }
            );          
    });

  });